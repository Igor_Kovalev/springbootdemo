package com.example.demo.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Configurer
        implements WebMvcConfigurer {

    @Value("${dev:false}")
    private Boolean dev;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (dev == Boolean.TRUE) {
            registry.addResourceHandler("/**")
                    .addResourceLocations("file:src/main/resources/no-min/");
        } else {
            registry.addResourceHandler("/**")
                    .addResourceLocations("file:src/main/resources/min/");
        }

    }

}



